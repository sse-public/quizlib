# QuizLib Plugin

## Content
The QuizLib Plugin for Dokuwiki embeds the JavaScript library QuizLib (https://alpsquid.github.io/quizlib/) of Andrew Palmer. The library offers functions to parse quizzes written in well-defined HTML syntax and to evaluate the user answers. Furthermore the plugin provide a simple XML-like syntax with a generic CSS styling as a all-round carefree package. Therefore 3 ways to write quizzes exist:
* XML syntax with generic styling and generic evaluation function (only one quiz per HTML page)
* HTML syntax with individual styling and generic evaluation function (only one quiz per HTML page)
* HTML syntax and individual evaluation function for total freedom

## Preconditions
The plugin is tested with:
* 2016-06-26a "Elenor of Tsort"
* 2015-08-10a "Detritus"

## Usage
There a three kinds of question formats you can use:
* plain text
* radio buttons (exactly one correct answer)
* checkbox (one or more correct answers)

### XML style
Every quiz starts with the tag "quizlib". This tag contains as attributes the array with the correct answers ("rightanswers") and the string for the submit button ("submit"). Each element of the rightanswers-array is either a string (for the plain text questions) or again an array with the zero-based indexes ('a0', 'a1', …) of the correct answers (for radio and checkbox). Please use double quotes for the rightanswers-array and single quotes inside.

The questions itself are child elements of the quizlib-tag. Each "question" has an attribute "title" with the content of the question and an attribute "type" (text, radio, checkbox). The possible answers are a list separated by a vertical bar. If you want to use characters that are protected in XML you have to work with entity references (http://www.w3schools.com/xml/xml_syntax.asp):
* `&lt;` for <
* `&gt;` for >
* `&amp;` for &
* `&apos;` for '
* `&quot;` for "

```xml
<quizlib id="quiz" rightanswers="['42',['a0'],['a0','a2','a3']]" submit="Check Answers">
    <question title="1. What is the answer to life, the universe and everything?" type="text"></question>
    <question title="2. Your enemy's father..." type="radio">is a hamster|smells of elderberries</question>
    <question title="3. Which factors will contribute to the end of humanity as we know it?" type="checkbox"> Global warming| The release of Linux 4.1.15| Cats| Advancements in artificial intelligence</question>
</quizlib>
```

### HTML style
The syntax is rather self-explanatory, if something is unclear read documentation of the JavaScript library https://alpsquid.github.io/quizlib/

The only difference to the original is the generic evaluation function "quizlibShowResults". It's the callback function for the onlick-event in the submit-button. Its first argument is the id of the quiz, the second is the array with the answers.

```html
<!-- Quiz Container -->
<div id="quiz">
    <!-- Question 1 -->
    <div class="quizlib-card quizlib-question">
        <div class="quizlib-question-title">1. What is the answer to life, the universe and everything?</div>
        <div class="quizlib-question-answers">
            <input type="text" name="q1">
        </div>
    </div>
    <!-- Question 2 -->
    <div class="quizlib-card quizlib-question">
        <div class="quizlib-question-title">2. Your enemy's father...</div>
        <div class="quizlib-question-answers">
            <ul>
                <li><label><input type="radio" name="q2" value="a"> is a hamster</label></li>
                <li><label><input type="radio" name="q2" value="b"> smells of elderberries</label></li>
            </ul>
        </div>
    </div>
    <!-- Question 3 -->
    <div class="quizlib-card quizlib-question">
        <div class="quizlib-question-title">3. Which factors will contribute to the end of humanity as we know it?</div>
        <div class="quizlib-question-answers">
            <ul>
                <li><label><input type="checkbox" name="q3" value="a"> Global warming</label></li>
                <li><label><input type="checkbox" name="q3" value="b"> The release of Linux 4.1.15</label></li>
                <li><label><input type="checkbox" name="q3" value="c"> Cats</label></li>
                <li><label><input type="checkbox" name="q3" value="d"> Advancements in artificial intelligence</label></li>
            </ul>
        </div>
    </div>
    <!-- Answer Button -->
    <button class="quizlib-submit" type="button" onclick="quizlibShowResults('quiz',['42',['a'],['a','c','d']]);">Check Answers</button>
</div>

<div id="quizlib-result" class="quizlib-card">
    You Scored <span id="quiz-percent"></span>% - <span id="quiz-score"></span>/<span id="quiz-max-score"></span><br/>
</div>
```

### Total freedom style
Like HTML style but you have to write your own evaluation function.

## Download
https://gitlab.hrz.tu-chemnitz.de/sse-public/quizlib/repository/archive.zip?ref=master

