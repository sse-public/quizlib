<?php
/**
* Plugin QuizLib: Implements quizes for self test on user side per JavaScript.
*
* @license    GPL 2 (http://www.gnu.org/licenses/gpl.html)
* @author     Enrico Billich <enrico.billich@etit.tu-chemnitz.de
*/
 
// must be run within Dokuwiki
if(!defined('DOKU_INC')) die();

if (!defined('DOKU_PLUGIN')) define('DOKU_PLUGIN',DOKU_INC.'lib/plugins/');
 
/**
 * All DokuWiki plugins to extend the parser/rendering mechanism
 * need to inherit from this class
 */
class syntax_plugin_quizlib extends DokuWiki_Syntax_Plugin {
	
	protected $pattern_start  = '<quizlib\b.*?>(?=.*?</quizlib>)';
    protected $pattern_end    = '</quizlib>';
    protected $quizattributes = array();
 
    public function getType(){ return 'container'; }
    public function getAllowedTypes() {return array();} //array('formatting', 'substition', 'disabled')
    public function getPType(){return 'block';}
    public function getSort(){ return 158; }
    
    public function connectTo($mode) {
        $this->Lexer->addEntryPattern($this->pattern_start, $mode, 'plugin_quizlib');
    }

    public function postConnect() {
        $this->Lexer->addExitPattern($this->pattern_end, 'plugin_quizlib');
        //$this->Lexer->addPattern($this->header_pattern, 'plugin_quizlib');
    }
 
 
    /**
     * Handle the match
     */
    public function handle($match, $state, $pos, Doku_Handler $handler){
        switch ($state) {
            case DOKU_LEXER_ENTER :
                $attributes = array();
                $xml        = simplexml_load_string(str_replace('>', '/>', $match));
                
                foreach ($xml->attributes() as $key => $value) {
                    $attributes[$key] = (string) $value;
                }
                
                $this->quizattributes = $attributes; // saved for DOKU_LEXER_EXIT
                return array($state, $attributes);
                 
            case DOKU_LEXER_UNMATCHED :
                return array($state, $match);
                
            case DOKU_LEXER_EXIT :
                return array($state, $this->quizattributes);
        }
        return array();
    }
 
    /**
     * Create output
     */
    public function render($mode, Doku_Renderer $renderer, $data) {
        // $data is what the function handle() return'ed.
        if($mode == 'xhtml'){
            /** @var Doku_Renderer_xhtml $renderer */
            list($state,$match) = $data;
            switch ($state) {
                case DOKU_LEXER_ENTER :
                    if(!isset($match['id'])) return false;
                    $renderer->doc .= sprintf('<div id="%s">', $match['id']) . PHP_EOL;
                    break;
 
                case DOKU_LEXER_UNMATCHED :
                    $xml = @simplexml_load_string('<root>' . $match . '</root>');
                    if (false === $xml) {
                        $renderer->doc .= 'Failed parsing quizlib-XML' . PHP_EOL;
                    }
                    else {
                        $questioncount = 0;
                        foreach ($xml->children() as $question){
                            $renderer->doc .= '<div class="quizlib-card quizlib-question">' . PHP_EOL;
                            foreach ($question->attributes() as $attribute=>$attributevalue){
                                switch ($attribute) {
                                    case 'title' :
                                        $renderer->doc .= '<div class="quizlib-question-title">' . $attributevalue . '</div>' . PHP_EOL;
                                        break;
                                    case 'type' :
                                        switch ($attributevalue) {
                                            case 'text' :
                                                $renderer->doc .= '<div class="quizlib-question-answers"><input type="text" name="q'.$questioncount.'"></div>' . PHP_EOL;
                                                break;
                                            case 'radio' :
                                                $renderer->doc .= '<div class="quizlib-question-answers"><ul>' . PHP_EOL;
                                                $answers = explode('|', $question);
                                                $answercount = 0;
                                                foreach ($answers as $answer){
                                                    $renderer->doc .= '<li><label><input type="radio" name="q'.$questioncount.'" value="a'.$answercount.'">'.$answer.'</label></li>' . PHP_EOL;
                                                    $answercount++;
                                                }
                                                $renderer->doc .= '</ul></div>' . PHP_EOL;
                                                break;
                                            case 'checkbox' :
                                                $renderer->doc .= '<div class="quizlib-question-answers"><ul>' . PHP_EOL;
                                                $answers = explode('|', $question);
                                                $answercount = 0;
                                                foreach ($answers as $answer){
                                                    $renderer->doc .= '<li><label><input type="checkbox" name="q'.$questioncount.'" value="a'.$answercount.'">'.$answer.'</label></li>' . PHP_EOL;
                                                    $answercount++;
                                                }
                                                $renderer->doc .= '</ul></div>' . PHP_EOL;
                                                break;
                                        }
                                        break;
                                }
                            }
                            $renderer->doc .= '</div>' . PHP_EOL;
                            $questioncount++;
                        }
                    }
                    break;
                case DOKU_LEXER_EXIT :
                    if(!isset($match['id']) or !isset($match['rightanswers']) or !isset($match['submit'])) return false;
                    $renderer->doc .= sprintf('<button class="quizlib-submit" type="button" onclick="quizlibShowResults(\'%s\',%s);">%s</button>',$match['id'],$match['rightanswers'],$match['submit']) . PHP_EOL;
                    $renderer->doc .= '</div>' . PHP_EOL;
                    $renderer->doc .= '<div id="quizlib-result" class="quizlib-card">' . PHP_EOL;
                    $renderer->doc .= 'You Scored <span id="quiz-percent"></span>% - <span id="quiz-score"></span>/<span id="quiz-max-score"></span><br/>' . PHP_EOL;
                    $renderer->doc .= '</div>' . PHP_EOL;
                    break;
            }
            return true;
        }
        return false;
    }

}
?>
